import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit  {
  srcImg  = "https://www.ngenespanol.com/wp-content/uploads/2018/08/Las-5-mejores-playas-desconocidas-en-M%C3%A9xico.jpg"
  titulo = 'Primera clase de ionic 5' 
 
   constructor(public router:Router) {
  
   }
 ngOnInit(){}
 goToPago(){

  let navigationExtras: NavigationExtras = {
    state: {
      mesage:'hola'
    }
  };
  this.router.navigate(['pago'], navigationExtras);
}
goToSocket(){

  let navigationExtras: NavigationExtras = {
    state: {
      mesage:'hola'
    }
  };
  this.router.navigate(['socket'], navigationExtras);
}
 }
 