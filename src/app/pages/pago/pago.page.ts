import { Component, OnInit } from '@angular/core';
import { Stripe } from '@ionic-native/stripe/ngx';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-pago',
  templateUrl: './pago.page.html',
  styleUrls: ['./pago.page.scss'],
})
export class PagoPage implements OnInit {

  num;
  monthexp;
  yearexp;
  cvv;
  token;
   options=[{nombre:"visa",id:"1"},
   {nombre:"american",id:"2"},
   {nombre:"otro",id:"3"}]
  tipo="2";
  title = 'app';
  elementType = 'url';
  value = 'Techiediaries';
    constructor(private stripe: Stripe, public http:HttpClient) {
  
    this.stripe.setPublishableKey('pk_test_51IBPIyIUQgkd7kx53dzxQGBe7NJVqAE2DI9DyCyzjILb0zCUQSl05oMUxEv8m66uajzSPhf11QwrCJl6rKhcLPva00Zy1eAIsE');
     }
  
     pagar(){
       var card = {
        number: this.num,
        expMonth: this.monthexp,
        expYear: this.yearexp,
        cvc: this.cvv
       }
       
    
      //  this.stripe.validateCardNumber(this.num)
      //  .then(car => alert('card:'+JSON.stringify(car)))
      //  .catch(error => alert('card err: '+error));
  
      //  this.stripe.validateCVC(this.cvv)
      //  .then(cc => alert('cvv: '+JSON.stringify(cc)))
      //  .catch(error => alert('cvv err: '+error));
  
      //  this.stripe.validateExpiryDate(this.monthexp,this.yearexp)
      //  .then(val => alert('exp: '+JSON.stringify(val)))
      //  .catch(error => alert('exp err: '+error));
       
       this.stripe.createCardToken(card)
       .then((token) => {
       alert('token: '+JSON.stringify(token))
       this.token= token.id
       this.post();
       }).catch((error) => {
        alert('token err: '+JSON.stringify(error))
       });
        
     }
  
     post(){
      var url = "https://payments-node.herokuapp.com/payment"
      this.http.post(url,{token:this.token} )
         .subscribe((data) => {
        //   var resp = data['response']
           alert('result: '+ JSON.stringify(data));
      },(error) => {
         alert('error: '+ JSON.stringify(error));
         });
     }
  
     ngOnInit(){}
  
    
  }
  